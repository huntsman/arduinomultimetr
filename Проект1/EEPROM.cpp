#include "EEPROM.h"

//EEPROM::EEPROM() {}

eeprom EEPROM;

unsigned char eeprom::read(int address)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryReader^ br = gcnew BinaryReader(fs);
	unsigned char cb = 0;

	br->BaseStream->Position = address;
	cb = br->ReadByte();

	fs->Close();
	return cb;
}

void eeprom::write(int address, char value)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryWriter^ bw = gcnew BinaryWriter(fs);
	unsigned char cb = 0;

	bw->BaseStream->Position = address;
	bw->Write(value);

	fs->Close();
}

void eeprom::put(int address, float data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryWriter^ bw = gcnew BinaryWriter(fs);
	unsigned char cb = 0;

	bw->BaseStream->Position = address;
	bw->Write(data);

	fs->Close();
}

void eeprom::put(int address, int data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryWriter^ bw = gcnew BinaryWriter(fs);
	unsigned char cb = 0;

	bw->BaseStream->Position = address;
	bw->Write((Int16)data);

	fs->Close();
}

void eeprom::put(int address, unsigned int data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryWriter^ bw = gcnew BinaryWriter(fs);
	unsigned char cb = 0;

	bw->BaseStream->Position = address;
	bw->Write((UInt16)data);

	fs->Close();
}

void eeprom::put(int address, long data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryWriter^ bw = gcnew BinaryWriter(fs);
	unsigned char cb = 0;

	bw->BaseStream->Position = address;
	bw->Write((Int32)data);

	fs->Close();
}

void eeprom::put(int address, unsigned long data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryWriter^ bw = gcnew BinaryWriter(fs);
	unsigned char cb = 0;

	bw->BaseStream->Position = address;
	bw->Write((UInt32)data);

	fs->Close();
}

void eeprom::get(int address, float &data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryReader^ br = gcnew BinaryReader(fs);
	float cb = 0.0f;

	br->BaseStream->Position = address;
	data = br->ReadSingle();

	fs->Close();
}

void eeprom::get(int address, int &data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryReader^ br = gcnew BinaryReader(fs);
	float cb = 0.0f;

	br->BaseStream->Position = address;
	data = br->ReadInt16();

	fs->Close();
}

void eeprom::get(int address, unsigned int &data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryReader^ br = gcnew BinaryReader(fs);
	float cb = 0.0f;

	br->BaseStream->Position = address;
	data = br->ReadUInt16();

	fs->Close();
}

void eeprom::get(int address, long &data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryReader^ br = gcnew BinaryReader(fs);
	float cb = 0.0f;

	br->BaseStream->Position = address;
	data = br->ReadInt32();

	fs->Close();
}

void eeprom::get(int address, unsigned long &data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryReader^ br = gcnew BinaryReader(fs);
	float cb = 0.0f;

	br->BaseStream->Position = address;
	data = br->ReadUInt32();

	fs->Close();
}

void eeprom::get(int address, bool &data)
{
	FileStream^ fs = gcnew FileStream(gcnew String(FileName), FileMode::OpenOrCreate);
	BinaryReader^ br = gcnew BinaryReader(fs);
	float cb = 0.0f;

	br->BaseStream->Position = address;
	data = br->ReadBoolean();

	fs->Close();
}

void eeprom::update(int address, float value)
{
	float tf = 0.0f;
	get(address, tf);
	if (tf != value)
		put(address, value);
}
