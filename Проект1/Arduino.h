#ifndef Arduino_h
#define Arduino_h

/*#pragma once
class Arduino
{
public:
	Arduino();
	~Arduino();

	int digitalRead(int Pin);
};
*/
#include "DeviceBase.h"
#include <stdio.h>
using namespace System;

#define  A7     21
#define  A6     20
#define  A5     19
#define  A4     18
#define  A3		17
#define  A2		16
#define  A1		15
#define  A0		14

#define  D13	13
#define  D12	12
#define  D11	11
#define  D10	10
#define  D9		9
#define  D8		8
#define  D7		7
#define  D6		6
#define  D5		5
#define  D4		4
#define  D3		3
#define  D2		2

static int a_adc(float Var)
{
	if (Var > 2)
		return 1;
	else if (Var < 0.8f)
		return 0;
	return 0;
}

static int digitalRead(int Pin)
{
	switch (Pin)
	{
	case A0:
		return a_adc(A0Variable);
		break;
	case A1:
		return a_adc(A1Variable);
		break;
	case A2:
		return a_adc(A2Variable);
		break;
	case A3:
		return a_adc(A3Variable);
		break;
	case A4:
		return a_adc(A4Variable);
		break;
	case A5:
		return a_adc(A5Variable);
		break;
	case A6:
		return a_adc(A6Variable);
		break;
	case A7:
		return a_adc(A7Variable);
		break;
	}
	return 0;
}

static int a_anre(float Var)
{
	return (int)(204.8f * Var);
}

static int analogRead(int Pin)
{
	switch (Pin)
	{
	case A0:
		return a_anre(A0Variable);
		break;
	case A1:
		return a_anre(A1Variable);
		break;
	case A2:
		return a_anre(A2Variable);
		break;
	case A3:
		return a_anre(A3Variable);
		break;
	case A4:
		return a_anre(A4Variable);
		break;
	case A5:
		return a_anre(A5Variable);
		break;
	case A6:
		return a_anre(A6Variable);
		break;
	case A7:
		return a_anre(A7Variable);
		break;
	}
	return 0;
}

static void digitalWrite(int pin, bool value)
{
	float val;
	if (value)
		val = 5.0f;
	else
		val = 0.0f;

	switch (pin)
	{
	case 14:
		A0Variable = val;
		break;
	case 15:
		A1Variable = val;
		break;
	case 16:
		A2Variable = val;
		break;
	case 17:
		A3Variable = val;
		break;
	case 18:
		A4Variable = val;
		break;
	case 19:
		A5Variable = val;
		break;
	case 20:
		A6Variable = val;
		break;
	case 21:
		A7Variable = val;
		break;

	case 2:
		D2Variable = val;
		break;
	case 3:
		D3Variable = val;
		break;
	case 4:
		D4Variable = val;
		break;
	case 5:
		D5Variable = val;
		break;
	case 6:
		D6Variable = val;
		break;
	case 7:
		D7Variable = val;
		break;
	case 8:
		D8Variable = val;
		break;
	case 9:
		D9Variable = val;
		break;
	case 10:
		D10Variable = val;
		break;
	case 11:
		D11Variable = val;
		break;
	case 12:
		D12Variable = val;
		break;
	case 13:
		D13Variable = val;
		break;
	}
}

static void analogWrite(int pin, int value)
{
	float val = value / 204.8f;
	if (val > 5)
		val = 5.0f;
	else if(val < 0)
		val = 0.0f;

	switch (pin)
	{
	case 14:
		A0Variable = val;
		break;
	case 15:
		A1Variable = val;
		break;
	case 16:
		A2Variable = val;
		break;
	case 17:
		A3Variable = val;
		break;
	case 18:
		A4Variable = val;
		break;
	case 19:
		A5Variable = val;
		break;
	case 20:
		A6Variable = val;
		break;
	case 21:
		A7Variable = val;
		break;

	case 2:
		D2Variable = val;
		break;
	case 3:
		D3Variable = val;
		break;
	case 4:
		D4Variable = val;
		break;
	case 5:
		D5Variable = val;
		break;
	case 6:
		D6Variable = val;
		break;
	case 7:
		D7Variable = val;
		break;
	case 8:
		D8Variable = val;
		break;
	case 9:
		D9Variable = val;
		break;
	case 10:
		D10Variable = val;
		break;
	case 11:
		D11Variable = val;
		break;
	case 12:
		D12Variable = val;
		break;
	case 13:
		D13Variable = val;
		break;
	}
}

static char * dtostrf(double val, signed char width, unsigned char prec, char * s)
{
	char ch[10] = { 0 };
	String ^ str = "%" + width.ToString() + "." + prec.ToString() + "f";
	//char* sth = "%" + Convert::ToChar(width) + ".2f";
	if (str->Length < sizeof(ch))
	sprintf(ch, "%s", str);
	sprintf(s, ch, val);
	return s;
}

static long millis()
{
	return MBTimer;
}

#endif 