using namespace System;
using namespace System::IO;

#define FileName "EEPROM.bin"



public class eeprom
{

public:
	unsigned char read(int address);
	void write(int address, char value);
	void put(int address, float data);
	void put(int address, int data);
	void put(int address, unsigned int data);
	void put(int address, long data);
	void put(int address, unsigned long data);
	void get(int address, float &data);
	void get(int address, int &data);
	void get(int address, unsigned int &data);
	void get(int address, long &data);
	void get(int address, unsigned long &data);
	void get(int address, bool &data);
	void update(int address, float value);


};

extern eeprom EEPROM;