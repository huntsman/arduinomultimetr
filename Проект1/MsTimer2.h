#ifndef MsTimer2_h
#define MsTimer2_h
#include "DeviceBase.h"

namespace MsTimer2 {

	extern void set(unsigned long ms, void(*f)());
	extern void start();
	extern void stop();
}

#endif