#pragma once
class LiquidCrystal
{
public:
	LiquidCrystal(void);
	~LiquidCrystal(void);
	LiquidCrystal(unsigned int on, unsigned int tw, unsigned int th, unsigned int fo, unsigned int fi, unsigned int si);

	void print(char *data);
	void print(float data, unsigned char prec);
	void print(unsigned char data);
	void print(int data);
	void print(unsigned int data);
	void write(int num);
	void clear();
	void begin(int cols, int rows);
	void setCursor(int col, int row);
	void createChar(int num, unsigned char *data);
	void createChar(int num, const unsigned char *data);
	char **Text;
	int Max_Column;
	int Max_Row;

private:

	int Sel_Column;
	int Sel_Row;
	int Temp_Len;
	
};

