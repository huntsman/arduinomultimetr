#include "../Firmware/Statistic.h"


Statistic::Statistic()
{

}


void Statistic::Collect()
{
	float pr, pr1;
	N_CurrentPower = N_Voltage * N_Current;
	pr = 150.0f / 3600000;
	pr1 = N_CurrentPower * pr;
	N_AllPower += pr1;

	if (LOAD_On) {
		pr1 = LOAD_Power * pr;
		N_ALoadPower = pr1;
	}
}

void Statistic::Save()
{
	unsigned int dcount;
	//unsigned long UDStat = 0;
	//unsigned int SdShift;
	int stat = 0;
	
	EEPROM.put(CUTAdress, time.UnixTime);
	stat = N_AllPower / 0.1f;
	EEPROM.put(APAdress, stat);

	stat = N_ALoadPower / 0.1f;
	EEPROM.put(ALPAdress, stat);

	dcount = time.DayDiff(N_UDStart);

	if (dcount >= 2) {
		x_tmd tmd;
		tmd.tm_sec = 0;
		tmd.tm_min = 0;
		tmd.tm_hour = 0;
		tmd.tm_mday = time.Day.GetValue();
		tmd.tm_mon = time.Month.GetValue();
		tmd.tm_year = time.Year.GetValue();
		N_UDStart = xtmtot(&tmd);
		EEPROM.put(SUTAdress, N_UDStart);
		dcount = 0;
	}

	//Insert current hour statistic to dataset
	stat = (N_AllPower - s_all_power) / 0.1f;
	N_DStat[dcount].HStat[time.Hours.GetValue()].AllPower = stat;

	stat = (N_ALoadPower - s_aload_power) / 0.1f;
	N_DStat[dcount].HStat[time.Hours.GetValue()].ALoadPower = stat;

	//Save current hour statistic to eeprom
	stat = HSStart + (HSSize * 24) * dcount;
	EEPROM.put(stat + 2 + HSSize * time.Hours.GetValue(), N_DStat[dcount].HStat[time.Hours.GetValue()].AllPower); // 2 - start offset(BUTSize), 6 - lenth of stat block
	EEPROM.put(stat + 4 + HSSize * time.Hours.GetValue(), N_DStat[dcount].HStat[time.Hours.GetValue()].ALoadPower); // 4 - start offset(BUTSize + s_all_power), 6 - lenth of stat block

	s_all_power = N_AllPower;
	s_aload_power = N_ALoadPower;
}

void Statistic::Load()
{
	int stat = 0;
	N_AllPower = 0;
	N_ALoadPower = 0;
	bool ForcClean;

	EEPROM.get(FCAdress, ForcClean);
	if (ForcClean)
		ForcedCleaning();

	EEPROM.get(CUTAdress, time.UnixTime);
	EEPROM.get(APAdress, stat);
	N_AllPower = stat * 0.1f;
	EEPROM.get(ALPAdress, stat);
	N_ALoadPower = stat * 0.1f;
	EEPROM.get(SUTAdress, N_UDStart);

	s_all_power = N_AllPower;
	s_aload_power = N_ALoadPower;
	time.SyncPull();

	for (int i = 0; i < SatsticDays; i++){
		stat = HSStart + (HSSize * 24) * i;
		for (int ii = 0; ii < 24; ii++){
			EEPROM.get(stat + 2 + HSSize * ii, N_DStat[i].HStat[ii].AllPower); // 2 - start offset(BUTSize), 6 - lenth of stat block
			EEPROM.get(stat + 4 + HSSize * ii, N_DStat[i].HStat[ii].ALoadPower); // 4 - start offset(BUTSize + s_all_power), 6 - lenth of stat block

		}
	}
}

void Statistic::ForcedCleaning()
{
	EEPROM.put(SSAdress, (float)0.027f);
	EEPROM.put(SSAdress + 4, (float)0.0f);
	EEPROM.put(SSAdress + 4 + 4, (float)0.047f);
	EEPROM.put(SSAdress + 4 + 4 + 4, (int)509);
	EEPROM.put(SSAdress + 4 + 4 + 4 + 2, (int)256);
	EEPROM.put(SSAdress + 4 + 4 + 4 + 2 + 2, (float)0.3f);
	EEPROM.put(SSAdress + 20, false);
	EEPROM.put(SSAdress + 20 + 1, false);
	EEPROM.put(SSAdress + 20 + 1 + 1, (int)600);
	EEPROM.put(SSAdress + 24, false);
	EEPROM.put(SSAdress + 24 + 1, (float)0.0f);
	EEPROM.put(SSAdress + 29, (int)4);

	int stat = 0;
	EEPROM.put(FCAdress, (bool)0);
	EEPROM.put(CUTAdress, (unsigned long)0);
	EEPROM.put(APAdress, (int)0);
	EEPROM.put(ALPAdress, (int)0);
	EEPROM.put(SUTAdress, (unsigned long)0);

	for (int i = 0; i < SatsticDays; i++){
		stat = HSStart + (HSSize * 24) * i;
		for (int ii = 0; ii < 24; ii++){
			EEPROM.put(stat + 2 + HSSize * ii, (int)0); // 2 - start offset(BUTSize), 6 - lenth of stat block
			EEPROM.put(stat + 4 + HSSize * ii, (int)0); // 4 - start offset(BUTSize + s_all_power), 6 - lenth of stat block
		}
	}
}
