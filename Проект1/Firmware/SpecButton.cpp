#include "../Firmware/SpecButton.h"

SpecButton::SpecButton(int pin, ButtonName Name, void (*Event)(ButtonEvent event))
{
	this->Pin = pin;
	this->Pushed = false;
	this->PushUpCounter = 0;
	this->PushDownCounter = 0;
	this->UpdatePeriod = 50;
	this->Event = Event;
	this->ThisEvent.Name = Name;
	this->ThisEvent.Event = BEventName::NONE;
}

void SpecButton::SetUpdatePeriod(int ms)
{
	UpdatePeriod = ms;
}

void SpecButton::Update()
{
	ReadedValue = (bool)digitalRead(Pin);

	if (ReadedValue)
	{
		PushUpCounter += UpdatePeriod;
		if (PushDownCounter != 0)
			PushDownCounter = 0;

		if (!Pushed)
		{
			ButtonDown();
			Pushed = true;
		}

		if (PushUpCounter >= LongAmt)
		{
			ThisEvent.Event = BEventName::LONG_PUSH;
			(*Event)(ThisEvent);

			Locking = true;
			PushUpCounter = 0;
		}
	}
	else
	{
		PushDownCounter += UpdatePeriod;
		if (PushDownCounter >= FilteredAmt)
		{

			if (Pushed)
			{
				ButtonUp();
				Pushed = false;
			}
			PushDownCounter = 0;
		}

		if (PushUpCounter != 0)
			PushUpCounter = 0;
	}


	/*

		switch (PushSwitch)
		{
		case BEventName::NONE:
			break;

		case BEventName::CLICK:
			ThisEvent.Event = BEventName::CLICK;
			(*Event)(ThisEvent);

			Pushed = false;
			break;

		case BEventName::LONG_PUSH:
			if (PushUpCounter >= LongAmt)
			{
				ThisEvent.Event = BEventName::LONG_PUSH;
				(*Event)(ThisEvent);

				Locking = true;
				PushUpCounter = 0;
			}
			break;
		}
		*/
}


void SpecButton::ButtonDown()
{
	if (Locking)
		Locking = false;
}

void SpecButton::ButtonUp()
{
	if (!Locking)
	{
		ThisEvent.Event = BEventName::CLICK;
		(*Event)(ThisEvent);
	}
	
}