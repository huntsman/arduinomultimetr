#ifndef StatisticScreen_h
#define StatisticScreen_h
/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/
#include "Screen.h"

#define GrafShift 1
#define CollMax 30

enum class selmode : int
{
	sel_none,
	sel_dataset,
	sel_data,
	sel_hour,
	sel_end
};

class StatisticScreen : public Screen
{
	public:
		StatisticScreen();
		void Draw();
		void ButtonAction(ButtonEvent event);
	private:
		void DrawColumn(unsigned char col, unsigned char fillpercent);
		void SymbolFill(int &percent);
};

#endif 