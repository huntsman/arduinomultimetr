#ifndef LimChar_h
#define LimChar_h

class LimChar
{
public:
	LimChar(void);
	LimChar(char min, char max);
	char GetValue();
	void SetValue(char val);
	void IncrValue(char inc);
	//int IncrValue(int inc);
private:
	char char_var;
	char char_max;
	char char_min;
};

#endif 