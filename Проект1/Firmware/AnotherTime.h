#include "xtime.h"
#include "LimInt.h"
#include "LimChar.h"

class AnotherTime
{
	struct etimer
	{
		void(*Event)();
		unsigned long Period;
		unsigned long LastMillis;
	};

public:
	unsigned long UnixTime;
	unsigned int DayDiff(unsigned long ut);

	void SyncPush();
	void SyncPull();
	void AddTimer(unsigned long millis, void(*event)());
	void Update(unsigned long millis);

	LimChar Seconds{ 0, 60 };
	LimChar Minutes{ 0, 60 };
	LimChar Hours{ 0, 23 };
	LimChar Day{ 1, 31 };
	LimChar Month{ 1, 12 };
	LimInt Year{ 1971, 2030 };

private:
	void ExMass();
	unsigned char TCount;
	unsigned long LastMillis;
	etimer *Timers;
	void IncrSec();
};

