#include "../Firmware/Adc.h"

float SVIValue;
float SCOValue;


Adc::Adc()
{
	SVIValue = 0;
	SCOValue = 0;
}

long t_Average;
float Average(int pin, int depth)
{
	t_Average = 0;
	for (int i = 0; i < depth; i++)
	{
		t_Average = t_Average + analogRead(pin);
	}
	return (t_Average / depth);
}

float Smoothing(float CurrentValue, float LastValue)
{
	if (LastValue == 0)
		LastValue = CurrentValue;
	return ((1 - ADC_SmoothCoof) * LastValue + ADC_SmoothCoof * CurrentValue);
}

void TranslaneValues()
{
	N_Voltage = (SVIValue - ADC_VoltZePo) * ADC_VoltMult;
	N_Current = (SCOValue - ADC_CurrZePo) * ADC_CurrMult;
}

void ReadValues()
{
	SVIValue = Smoothing(Average(VI_PIN, ADC_AverDepth), SVIValue);
	SCOValue = Smoothing(Average(CO_PIN, ADC_AverDepth), SCOValue);

	TranslaneValues();
}

void Adc::Update()
{
	ReadValues();
}