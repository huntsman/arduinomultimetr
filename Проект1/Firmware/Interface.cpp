#include "../Firmware/Interface.h"

Screen *Screens[SCREEN_COUNT];

int ScreenIndex = 0;
bool lcdleden = false;
void LcdLedwitch(bool on);

void BEvent(ButtonEvent event)
{
	lcd.clear();
	if (Screens[ScreenIndex]->InterControl)
		Screens[ScreenIndex]->ButtonAction(event);
	else
		switch (event.Name)
		{
		case ButtonName::BUTTON_OK:
			if (event.Event == BEventName::CLICK) {
				lcdleden = !lcdleden;
				LcdLedwitch(lcdleden);
			}
				
			if (event.Event == BEventName::LONG_PUSH)
			{
				//���������� ����
				//Screens[2]->Hide = false;
				ScreenIndex = 3;
				/////////////////
			}
			break;

		case ButtonName::BUTTON_UP:
			if (event.Event == BEventName::CLICK)
				if (ScreenIndex < SCREEN_COUNT - 2) //ScreenIndex + 1
					++ScreenIndex;

			break;

		case ButtonName::BUTTON_DOWN:
			if (event.Event == BEventName::CLICK)
				if (ScreenIndex > 0)
					--ScreenIndex;

			break;
		}
}

SpecButton OkButton(BUTTON_OK_PIN, ButtonName::BUTTON_OK, BEvent);
SpecButton UpButton(BUTTON_UP_PIN, ButtonName::BUTTON_UP, BEvent);
SpecButton DownButton(BUTTON_DOWN_PIN, ButtonName::BUTTON_DOWN, BEvent);

Interface::Interface()
{
	ScreenIndex = 0;
	//Screens = new Screen[3];
	Screens[0] = new MainScreenOne();
	Screens[1] = new MainScreenTwo();
	Screens[2] = new StatisticScreen();
	Screens[3] = new DeviceMenu();
	

}

void LcdLedwitch(bool on)
{
	if (on) {
		if (LCD_LedEnable)
			analogWrite(PWM_LCD_LED_PIN, LCD_Bright.GetValue());
	}
	else
		analogWrite(PWM_LCD_LED_PIN, 0);

}







void Interface::DrawUpdate()
{
	if(Screens[ScreenIndex]->Hide == false)
		Screens[ScreenIndex]->Draw();
	else {
		Screens[ScreenIndex]->Hide = false;
		ScreenIndex = 0;
	}
}

void Interface::InputUpdate()
{
	OkButton.Update();
	UpButton.Update();
	DownButton.Update();
}


