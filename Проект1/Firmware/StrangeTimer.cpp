#include "../Firmware/StrangeTimer.h"

StrangeTimer::StrangeTimer()
{
	this->Time = 0;
	this->Event = nullptr;
	this->LastMillis = 0;
	this->StopTimer = true;
	init = true;
}

StrangeTimer::StrangeTimer(unsigned long millis, void (*event)())
{
	Time = millis;
	Event = event; 
	LastMillis = 0;
	StopTimer = true;
	init = true;
}

void StrangeTimer::Start()
{
	StopTimer = false;
}

void StrangeTimer::Stop()
{
	StopTimer = true;
	LastMillis = 0;
}

void StrangeTimer::SetPeriod(unsigned long millis)
{
	Time = millis;
}

void StrangeTimer::SetEvent(void(*event)())
{
	Event = event;
}

void StrangeTimer::Update(unsigned long millis)
{
	if(!StopTimer)
	{
		if (millis < LastMillis)
			LastMillis = millis;
		
		if(LastMillis == 0)
			LastMillis= millis;
		

		if (millis - LastMillis >= Time)
		{
			LastMillis = millis;
			if (Event != nullptr)
				(*Event)();
		}
	}  	
}
