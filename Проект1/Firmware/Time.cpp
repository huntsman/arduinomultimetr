#include "../Firmware/Time.h"
#include "../MsTimer2.h"

//void Update();
char MaxMonth;
char MaxDays;

unsigned long Time::UnixTime = 0;
LimChar Time::Seconds(0, 60);
LimChar Time::Minutes(0, 60);
LimChar Time::Hours(0, 24);
LimChar Time::Day(1, 31);
LimChar Time::Month(1, 12);
LimInt Time::Year(1971, 2030);

void (*Time::HourEvent)() = nullptr;

#define	isLeapYear(year)	((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
#define	GetMaxDays(year, mon)	((28 + ((0x3bbeecc >> (mon * 2)) & 3)) + isLeapYear(year))

Time::Time()
{
	MsTimer2::set(50, &Update);
	MsTimer2::start();

	/*Time::Seconds = 0;
	Time::Minutes = 0;
	Time::Hours = 0;
	Time::Day = 1;
	Time::Month = 1;
	Time::Year = 1900;*/

	MaxDays = GetMaxDays(Time::Year.GetValue(), Time::Month.GetValue());
}

void Time::SetHourEvent(void(*he)())
{
	HourEvent = he;
}

void Time::Save()
{
	x_tmd tmd;
	tmd.tm_sec = Time::Seconds.GetValue();
	tmd.tm_min = Time::Minutes.GetValue();
	tmd.tm_hour = Time::Hours.GetValue();
	tmd.tm_mday = Time::Day.GetValue();
	tmd.tm_mon = Time::Month.GetValue();
	tmd.tm_year = Time::Year.GetValue();
	UnixTime = xtmtot(&tmd);
}

void Time::Load()
{
	x_tmd tmd;
	xttotm(&tmd, UnixTime);
	Time::Seconds.SetValue(tmd.tm_sec);
	Time::Minutes.SetValue(tmd.tm_min);
	Time::Hours.SetValue(tmd.tm_hour);
	Time::Day.SetValue(tmd.tm_mday);
	Time::Month.SetValue(tmd.tm_mon);
	Time::Year.SetValue(tmd.tm_year);
}

void Time::Update()
{
	Time::Seconds.IncrValue(1);
	++Time::UnixTime;
	if (Time::Seconds.GetValue() > 59) {
		Time::Seconds.SetValue(0);
		IncrMin();
	}
}

void Time::IncrMin()
{
	Time::Minutes.IncrValue(1);
	if (Time::Minutes.GetValue() > 59) {
		Time::Minutes.SetValue(0);
		IncrHours();
	}
}
void Time::IncrHours()
{
	Time::Hours.IncrValue(1);
	if (Time::Hours.GetValue() > 23) {
		Time::Hours.SetValue(0);
		IncrDay();
	}
	Save();
	if (HourEvent != nullptr)
		(*HourEvent)();
}
void Time::IncrDay()
{
	Time::Day.IncrValue(1);
	if (Time::Day.GetValue() > MaxDays) {
		Time::Day.SetValue(1);
		IncrMonth();
	}
}
void Time::IncrMonth()
{
	Time::Month.IncrValue(1);
	if (Time::Month.GetValue() > 12) {
		Time::Month.SetValue(1);
		IncrYear();
	}
}
void Time::IncrYear()
{
	Time::Year.IncrValue(1);
}