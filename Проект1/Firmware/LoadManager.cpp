#include "../Firmware/LoadManager.h"

LoadManager::LoadManager()
{
}

void SwitchLoad(bool en)
{
	if (!LOAD_On)
		if (en) {
			digitalWrite(LOAD_SWITCH_PIN, en);
			LOAD_On = en;
		}
}

void LoadManager::Update()
{
	if (LOAD_Power > N_CurrentPower) {
		SwitchLoad(false);
	}

	if (N_Voltage > 13)
		SwitchLoad(true);

}