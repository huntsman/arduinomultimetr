#ifndef MainScreenTwo_h
#define MainScreenTwo_h
/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/
#include "Screen.h"

class MainScreenTwo : public Screen
{
	public:
		MainScreenTwo();
		void Draw();
		void ButtonAction(ButtonEvent event);

};

#endif 