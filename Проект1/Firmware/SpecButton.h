#ifndef SpecButton_h
#define SpecButton_h
/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/
#define LongAmt 1000 // 1s
#define ClickFilter LongAmt/3 // ����������� > UpdatePeriod
#define FilteredAmt 200 // 0.1s

#include "../Firmware/Base.h"
//#include "../Arduino.h"

class SpecButton
{
	public:
		SpecButton(int pin, ButtonName Name, void (*Event)(ButtonEvent event));
		void SetUpdatePeriod(int ms);
		void Update();
	private:
		int Pin;
		bool Pushed;
		bool Locking;
		ButtonEvent ThisEvent;
		void (*Event)(ButtonEvent event);
		long PushUpCounter;
		long PushDownCounter;
		int  UpdatePeriod;
		bool ReadedValue;
		BEventName PushSwitch;
		void ButtonDown();
		void ButtonUp();
};


#endif // #ifndef buttons_h