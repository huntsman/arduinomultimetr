#ifndef Time_h
#define Time_h


/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/
#include "../Firmware/xtime.h"
#include "LimChar.h"
#include "LimInt.h"

class Time
{

public:
	Time();
	static void	Update();
	static unsigned long	UnixTime;
	static LimChar	Seconds;
	static LimChar	Minutes;
	static LimChar	Hours;
	static LimChar	Day;
	static LimChar	Month;
	static LimInt	Year;
	static void	SetHourEvent (void(*he)());
	static void	Save();
	static void	Load();

private:
	static void IncrMin();
	static void IncrHours();
	static void IncrDay();
	static void IncrMonth();
	static void IncrYear();
	static void (*HourEvent)();
};

#endif 