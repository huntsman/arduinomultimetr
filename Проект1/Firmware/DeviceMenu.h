#ifndef DeviceMenu_h
#define DeviceMenu_h
/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/

#include "Screen.h"

enum class ElementType :char
{
	NONE,           //Заглушка
	TYPE_BACK,
	TYPE_ROOT,
	TYPE_NEXT,
	TYPE_TEXT,
	TYPE_EXTERNAL,
	TYPE_VARIABLE
};

enum class VariableType :char
{
	NONE,           //Заглушка
	TYPE_INT,
	TYPE_BOOL,
	TYPE_VOID,
	TYPE_FLOAT,
	TYPE_LIM_INT,
	TYPE_LIM_CHAR
};

struct MenuElement
{         
	MenuElement *Previous;
	MenuElement *Items; //Массив из MenuElement
	int ItemsCount;
	char* Text;
	int TextSize;
	ElementType ElType;
	void* Variable;
	VariableType VarType;
}; 


class DeviceMenu : public Screen
{

	public:
		DeviceMenu();
		void BuildMenu();
		void ButtonAction(ButtonEvent event);
		void Draw();
	private:
		void Processing(MenuElement *me, int number, bool Sel);
		MenuElement DMenu;
		int SelectedItem;
		int MenuShift;
		//int MenuUpBorder;
		//int MenuDownBorder;
		unsigned char EditIncr;
	
		void SelectItem();
		void MovingUp();
		void MovingDown();
		//void BuildMenu();

		float RFloatIncr();
		bool RBoolIncr();
		int RIntIncr();
		char* ReturnIncr();
		void MenuReset();
		void IncrValue(bool plus);
		void PrintVariable(MenuElement *me);
		void EditVariable(MenuElement *me);
};

#endif 