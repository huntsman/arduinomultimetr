typedef	unsigned short			u16_t;
typedef	unsigned long			u32_t;
typedef	unsigned char			byte;
typedef	signed short			s16_t;
typedef	signed long			s32_t;

// DEF: standard signed format
// UNDEF: non-standard unsigned format
//#define	_XT_SIGNED

#ifdef	_XT_SIGNED
typedef	s32_t                           xtime_t;
#else
typedef	u32_t                           xtime_t;
#endif

struct x_tmd
{       /* date and time components */
	byte	tm_sec;
	byte	tm_min;
	byte	tm_hour;
	byte	tm_mday;
	byte	tm_mon;
	u16_t	tm_year;
};

void xttotm(struct x_tmd *t, xtime_t secs);
xtime_t xtmtot(struct x_tmd *t);