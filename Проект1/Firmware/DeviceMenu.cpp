#include "../Firmware/DeviceMenu.h"


DeviceMenu::DeviceMenu()
{
	InterControl = true;
	EditIncr = 0;
	MenuReset();
	BuildMenu();
}

void DeviceMenu::MenuReset()
{
	SelectedItem = 0;
	MenuShift = 0;
}

char * DeviceMenu::ReturnIncr()
{
	switch (DMenu.Items[SelectedItem].VarType)
	{
	case VariableType::TYPE_FLOAT:
		dtostrf(RFloatIncr(), 7, 3, Buffer);
		//sprintf(Buffer, "%g", RFloatIncr());
		break;

	case VariableType::TYPE_INT:
		sprintf(Buffer, "%d", RIntIncr());
		break;

	case VariableType::TYPE_LIM_INT:
		sprintf(Buffer, "%d", RIntIncr());
		break;

	case VariableType::TYPE_LIM_CHAR:
		sprintf(Buffer, "%d", RIntIncr());
		break;

	case VariableType::TYPE_BOOL:
		if (RBoolIncr()) {
			sprintf(Buffer, "%s", "true");
		}
		else {
			sprintf(Buffer, "%s", "false");
		}
		break;
	}
	return Buffer;
}

void DeviceMenu::IncrValue(bool plus)
{
	float val;
	switch (DMenu.Items[SelectedItem].VarType)
	{

	case VariableType::TYPE_FLOAT:
		val = RFloatIncr();
		if (!plus)
			val *= -1;
		*(float*)DMenu.Items[SelectedItem].Variable += val;
		break;

	case VariableType::TYPE_INT:
		val = RIntIncr();
		if (!plus)
			val *= -1;
		*(int*)DMenu.Items[SelectedItem].Variable += val;
		break;
	case VariableType::TYPE_BOOL:
		if (RBoolIncr())
			*(bool*)DMenu.Items[SelectedItem].Variable = true;
		else
			*(bool*)DMenu.Items[SelectedItem].Variable = false;
		break;
	case VariableType::TYPE_LIM_INT:
		val = RIntIncr();
		if (!plus)
			val *= -1;
		(*(LimInt*)DMenu.Items[SelectedItem].Variable).IncrValue((int)val);
		break;
	case VariableType::TYPE_LIM_CHAR:
		val = RIntIncr();
		if (!plus)
			val *= -1;
		(*(LimChar*)DMenu.Items[SelectedItem].Variable).IncrValue((char)val);
		break;
	}
}

int DeviceMenu::RIntIncr()
{
	if (EditIncr > 3)
		EditIncr = 1;
	switch (EditIncr)
	{
	case 1:
		return 1;
		break;
	case 2:
		return 10;
		break;
	case 3:
		return 100;
		break;
	}
	return 0;
}

bool DeviceMenu::RBoolIncr()
{
	if (EditIncr > 2)
		EditIncr = 1;
	switch (EditIncr)
	{
	case 1:
		return true;
		break;
	case 2:
		return false;
		break;
	}
	return false;
}


float DeviceMenu::RFloatIncr()
{
	if (EditIncr > 5)
		EditIncr = 1;
	switch (EditIncr)
	{
	case 1:
		return 10.0f;
		break;
	case 2:
		return 1.0f;
		break;
	case 3:
		return 0.1f;
		break;
	case 4:
		return 0.01f;
		break;
	case 5:
		return 0.001f;
		break;
	}
	return 0;
}

void  DeviceMenu::SelectItem()
{
	switch (DMenu.Items[SelectedItem].ElType)
	{

	case ElementType::TYPE_VARIABLE:
		EditIncr += 1;
		break;

	case ElementType::TYPE_NEXT:
		DMenu = DMenu.Items[SelectedItem];
		MenuReset();
		break;

	case ElementType::TYPE_BACK:
		if (DMenu.Previous != NULL)
			DMenu = *DMenu.Previous;
		else
			Hide = true;

		MenuReset();
		break;

	case ElementType::TYPE_EXTERNAL:
		if (DMenu.Items[SelectedItem].Variable != NULL) {
			(*(void(*)())DMenu.Items[SelectedItem].Variable)();
			SelectedItem = 0;
			MenuShift = 0;
		}
		break;
	}
}



void DeviceMenu::MovingUp()
{
	if (EditIncr > 0) {
		IncrValue(true);
	}
	else{
		if (SelectedItem > 0)
			--SelectedItem;

		if (SelectedItem < MenuShift){
			--MenuShift;
		}
	}
}

void DeviceMenu::MovingDown()
{
	if (EditIncr > 0) {
		IncrValue(false);
	}
	else {
		if (SelectedItem < DMenu.ItemsCount - 1)
			SelectedItem += 1;

		if (SelectedItem + 1 > MenuShift + DisplayHeight){
			++MenuShift;
		}
	}
}

void DeviceMenu::ButtonAction(ButtonEvent event)
{
	//lcd.clear();
	switch(event.Name)
	{
		case ButtonName::BUTTON_OK:
			if(event.Event == BEventName::CLICK)
				SelectItem();
			else if (event.Event == BEventName::LONG_PUSH){
				if (EditIncr > 0)
					EditIncr = 0;
			}
		break;
    
		case ButtonName::BUTTON_UP:
			if(event.Event == BEventName::CLICK)
				MovingUp();
		break;
		
		case ButtonName::BUTTON_DOWN:
			if(event.Event == BEventName::CLICK)
				MovingDown();
		break;
	}
}

void DeviceMenu::Draw()
{
	bool sel;
	if (EditIncr == 0) {
		if (DMenu.ItemsCount > 0)
			for (int i = MenuShift; i < MenuShift + DisplayHeight; i++)
			{
				sel = (i == SelectedItem ? true : false);
				if (i - MenuShift < DMenu.ItemsCount)
					Processing(&DMenu.Items[i], i - MenuShift, sel);
			}
	}
	else
		EditVariable(&DMenu.Items[SelectedItem]);
}

void DeviceMenu::Processing(MenuElement *me, int number, bool Sel)
{
		if (Sel) {
			lcd.setCursor(0, number);
			lcd.write(1);
		}
		lcd.setCursor(1, number);
		lcd.print(me->Text);
		if (me->ElType == ElementType::TYPE_VARIABLE) {
			lcd.setCursor(me->TextSize + 1, number); // +1 селектор
			lcd.print(":");
			lcd.setCursor(me->TextSize + 2, number); // +1 селектор
			PrintVariable(me);
		}

}

void DeviceMenu::EditVariable(MenuElement *me)
{
	if (DisplayWidth >= me->TextSize) {
		lcd.setCursor(0, 0);
		lcd.print(me->Text);
		lcd.setCursor(me->TextSize, 0);
		lcd.print(":");
		lcd.setCursor(me->TextSize + 1, 0);
		PrintVariable(me);
		lcd.setCursor(0, 1);
		sprintf(Buffer, "%s", "Choise:");
		lcd.print(Buffer);
		lcd.setCursor(7, 1);
		lcd.print(ReturnIncr());
	}
}

void DeviceMenu::PrintVariable(MenuElement *me)
{
	if (me->ElType == ElementType::TYPE_VARIABLE)
		switch (me->VarType)
		{
		case VariableType::TYPE_INT:
			sprintf(Buffer, "%d", *(int*)me->Variable);
			lcd.print(Buffer);
			break;

		case VariableType::TYPE_FLOAT:
			lcd.print(*(float*)me->Variable, 3);
			break;

		case VariableType::TYPE_BOOL:
			if (*(float*)me->Variable) {
				sprintf(Buffer, "%s", "true");
				lcd.print(Buffer);
			}
			else {
				sprintf(Buffer, "%s", "false");
				lcd.print(Buffer);
			}
			break;
		case VariableType::TYPE_VOID:
			sprintf(Buffer, "%s", "void");
			lcd.print(Buffer);
			break;
		case VariableType::TYPE_LIM_INT:
			sprintf(Buffer, "%d", (*(LimInt*)me->Variable).GetValue());
			lcd.print(Buffer);
			break;
		case VariableType::TYPE_LIM_CHAR:
			sprintf(Buffer, "%d", (*(LimChar*)me->Variable).GetValue());
			lcd.print(Buffer);
			break;
		}
}

void AdcSettSave()
{
	EEPROM.put(SSAdress, ADC_VoltMult);
	EEPROM.put(SSAdress + 4, ADC_VoltZePo);
	EEPROM.put(SSAdress + 4 + 4, ADC_CurrMult);
	EEPROM.put(SSAdress + 4 + 4 + 4, ADC_CurrZePo);
	EEPROM.put(SSAdress + 4 + 4 + 4 + 2, ADC_AverDepth);
	EEPROM.put(SSAdress + 4 + 4 + 4 + 2 + 2, ADC_SmoothCoof);
}

void LcdSettSave()
{
	EEPROM.put(SSAdress + 20, LCD_LedOnStart);
	EEPROM.put(SSAdress + 20 + 1, LCD_LedEnable);
	EEPROM.put(SSAdress + 20 + 1 + 1, LCD_Bright.GetValue());
}

void LoadSettSave()
{
	EEPROM.put(SSAdress + 24, LOAD_On);
	EEPROM.put(SSAdress + 24 + 1, LOAD_Power);
}

void StatSettSave()
{
	EEPROM.put(SSAdress + 29, STAT_ShStep);
}

void TME_Apply()
{
	time.Seconds.SetValue(0);
	time.SyncPush();
	EEPROM.put(CUTAdress, time.UnixTime);
}

void StatErase()
{
	EEPROM.put(0, true);
}

void DeviceMenu::BuildMenu()
{

	//Корень дерева
	MenuElement *RootME = new MenuElement[1];
	RootME[0].ItemsCount = 6;
	RootME[0].TextSize = 0;
	RootME[0].ElType = ElementType::TYPE_ROOT;
	RootME[0].Previous = NULL;

	MenuElement *SetME = new MenuElement[6];
	//ToMain
	SetME[0].Previous = &RootME[0];
	SetME[0].ItemsCount = 0;
	SetME[0].Text = "Main";
	SetME[0].TextSize = 4;
	SetME[0].ElType = ElementType::TYPE_BACK;
	//Adc
	SetME[1].Previous = &RootME[0];
	SetME[1].Text = "Adc";
	SetME[1].TextSize = 3;
	SetME[1].ElType = ElementType::TYPE_NEXT;
	SetME[1].ItemsCount = 8;
	//Lcd
	SetME[2].Previous = &RootME[0];
	SetME[2].Text = "Lcd";
	SetME[2].TextSize = 3;
	SetME[2].ElType = ElementType::TYPE_NEXT;
	SetME[2].ItemsCount = 5;
	//Load
	SetME[3].Previous = &RootME[0];
	SetME[3].Text = "Load";
	SetME[3].TextSize = 4;
	SetME[3].ElType = ElementType::TYPE_NEXT;
	SetME[3].ItemsCount = 3;
	//Time
	SetME[4].Previous = &RootME[0];
	SetME[4].Text = "Time";
	SetME[4].TextSize = 4;
	SetME[4].ElType = ElementType::TYPE_NEXT;
	SetME[4].ItemsCount = 8;
	//Stat
	SetME[5].Previous = &RootME[0];
	SetME[5].Text = "Stat";
	SetME[5].TextSize = 4;
	SetME[5].ElType = ElementType::TYPE_NEXT;
	SetME[5].ItemsCount = 4;

	//Adc
	MenuElement *AdcME = new MenuElement[8];
	//Adc->Back
	AdcME[0].Previous = &SetME[1];
	AdcME[0].Text = "Back";
	AdcME[0].TextSize = 4;
	AdcME[0].ItemsCount = 0;
	AdcME[0].ElType = ElementType::TYPE_BACK;
	//Adc->VoltMult
	AdcME[1].Previous = &SetME[1];
	AdcME[1].Text = "VoltMult";
	AdcME[1].TextSize = 8;
	AdcME[1].ItemsCount = 0;
	AdcME[1].ElType = ElementType::TYPE_VARIABLE;
	AdcME[1].Variable = &ADC_VoltMult;
	AdcME[1].VarType = VariableType::TYPE_FLOAT;
	//Adc->VoltZePo
	AdcME[2].Previous = &SetME[1];
	AdcME[2].Text = "VoltZePo";
	AdcME[2].TextSize = 8;
	AdcME[2].ItemsCount = 0;
	AdcME[2].ElType = ElementType::TYPE_VARIABLE;
	AdcME[2].Variable = &ADC_VoltZePo;
	AdcME[2].VarType = VariableType::TYPE_FLOAT;
	//Adc->CurrMult
	AdcME[3].Previous = &SetME[1];
	AdcME[3].Text = "CurrMult";
	AdcME[3].TextSize = 8;
	AdcME[3].ItemsCount = 0;
	AdcME[3].ElType = ElementType::TYPE_VARIABLE;
	AdcME[3].Variable = &ADC_CurrMult;
	AdcME[3].VarType = VariableType::TYPE_FLOAT;
	//Adc->CurrZePo
	AdcME[4].Previous = &SetME[1];
	AdcME[4].Text = "CurrZePo";
	AdcME[4].TextSize = 8;
	AdcME[4].ItemsCount = 0;
	AdcME[4].ElType = ElementType::TYPE_VARIABLE;
	AdcME[4].Variable = &ADC_CurrZePo;
	AdcME[4].VarType = VariableType::TYPE_INT;
	//Adc->AverDepth
	AdcME[5].Previous = &SetME[1];
	AdcME[5].Text = "AverDepth";
	AdcME[5].TextSize = 9;
	AdcME[5].ItemsCount = 0;
	AdcME[5].ElType = ElementType::TYPE_VARIABLE;
	AdcME[5].Variable = &ADC_AverDepth;
	AdcME[5].VarType = VariableType::TYPE_INT;
	//Adc->SmoothCoof
	AdcME[6].Previous = &SetME[1];
	AdcME[6].Text = "SmoothCoof";
	AdcME[6].TextSize = 10;
	AdcME[6].ItemsCount = 0;
	AdcME[6].ElType = ElementType::TYPE_VARIABLE;
	AdcME[6].Variable = &ADC_SmoothCoof;
	AdcME[6].VarType = VariableType::TYPE_FLOAT;
	//Adc->Save
	AdcME[7].Previous = &SetME[1];
	AdcME[7].Text = "Save";
	AdcME[7].TextSize = 4;
	AdcME[7].ItemsCount = 0;
	AdcME[7].ElType = ElementType::TYPE_EXTERNAL;
	AdcME[7].Variable = &AdcSettSave;
	AdcME[7].VarType = VariableType::TYPE_VOID;
	//Добавляем детей Adc
	SetME[1].Items = AdcME;

	//Lcd
	MenuElement *LcdME = new MenuElement[5];
	//Lcd->Back
	LcdME[0].Previous = &SetME[2];
	LcdME[0].Text = "Back";
	LcdME[0].TextSize = 4;
	LcdME[0].ElType = ElementType::TYPE_BACK;
	LcdME[0].ItemsCount = 0;
	//Lcd->OnStart
	LcdME[1].Previous = &SetME[2];
	LcdME[1].Text = "OnStart";
	LcdME[1].TextSize = 7;
	LcdME[1].ItemsCount = 0;
	LcdME[1].ElType = ElementType::TYPE_VARIABLE;
	LcdME[1].Variable = &LCD_LedOnStart;
	LcdME[1].VarType = VariableType::TYPE_BOOL;
	//Lcd->Enable
	LcdME[2].Previous = &SetME[2];
	LcdME[2].Text = "Enable";
	LcdME[2].TextSize = 6;
	LcdME[2].ItemsCount = 0;
	LcdME[2].ElType = ElementType::TYPE_VARIABLE;
	LcdME[2].Variable = &LCD_LedEnable;
	LcdME[2].VarType = VariableType::TYPE_BOOL;
	//Lcd->Bright
	LcdME[3].Previous = &SetME[2];
	LcdME[3].Text = "Bright";
	LcdME[3].TextSize = 6;
	LcdME[3].ItemsCount = 0;
	LcdME[3].ElType = ElementType::TYPE_VARIABLE;
	LcdME[3].Variable = &LCD_Bright;
	LcdME[3].VarType = VariableType::TYPE_LIM_INT;
	//Lcd->Save
	LcdME[4].Previous = &SetME[2];
	LcdME[4].Text = "Save";
	LcdME[4].TextSize = 4;
	LcdME[4].ItemsCount = 0;
	LcdME[4].ElType = ElementType::TYPE_EXTERNAL;
	LcdME[4].Variable = &LcdSettSave;
	LcdME[4].VarType = VariableType::TYPE_VOID;
	//Добавляем детей Counter
	SetME[2].Items = LcdME;

	//Load
	MenuElement *LoadME = new MenuElement[3];
	//Load->Back
	LoadME[0].Previous = &SetME[3];
	LoadME[0].Text = "Back";
	LoadME[0].TextSize = 4;
	LoadME[0].ElType = ElementType::TYPE_BACK;
	LoadME[0].ItemsCount = 0;
	//Load->Power
	LoadME[1].Previous = &SetME[3];
	LoadME[1].Text = "Power";
	LoadME[1].TextSize = 5;
	LoadME[1].ItemsCount = 0;
	LoadME[1].ElType = ElementType::TYPE_VARIABLE;
	LoadME[1].Variable = &LOAD_Power;
	LoadME[1].VarType = VariableType::TYPE_FLOAT;
	//Load->Save
	LoadME[2].Previous = &SetME[3];
	LoadME[2].Text = "Save";
	LoadME[2].TextSize = 4;
	LoadME[2].ItemsCount = 0;
	LoadME[2].ElType = ElementType::TYPE_EXTERNAL;
	LoadME[2].Variable = &LoadSettSave;
	LoadME[2].VarType = VariableType::TYPE_VOID;
	//Добавляем детей Load
	SetME[3].Items = LoadME;

	//Time
	MenuElement *TimeME = new MenuElement[8];
	//Time->Back
	TimeME[0].Previous = &SetME[4];
	TimeME[0].Text = "Back";
	TimeME[0].TextSize = 4;
	TimeME[0].ElType = ElementType::TYPE_BACK;
	TimeME[0].ItemsCount = 0;
	//Time->Hour
	TimeME[1].Previous = &SetME[4];
	TimeME[1].Text = "Hour";
	TimeME[1].TextSize = 4;
	TimeME[1].ItemsCount = 0;
	TimeME[1].ElType = ElementType::TYPE_VARIABLE;
	TimeME[1].Variable = &time.Hours;
	TimeME[1].VarType = VariableType::TYPE_LIM_CHAR;
	//Time->Minute
	TimeME[2].Previous = &SetME[4];
	TimeME[2].Text = "Minute";
	TimeME[2].TextSize = 6;
	TimeME[2].ItemsCount = 0;
	TimeME[2].ElType = ElementType::TYPE_VARIABLE;
	TimeME[2].Variable = &time.Minutes;
	TimeME[2].VarType = VariableType::TYPE_LIM_CHAR;
	//Time->Seconds
	TimeME[3].Previous = &SetME[4];
	TimeME[3].Text = "Seconds";
	TimeME[3].TextSize = 7;
	TimeME[3].ItemsCount = 0;
	TimeME[3].ElType = ElementType::TYPE_VARIABLE;
	TimeME[3].Variable = &time.Seconds;
	TimeME[3].VarType = VariableType::TYPE_LIM_CHAR;
	//Time->Day
	TimeME[4].Previous = &SetME[4];
	TimeME[4].Text = "Day";
	TimeME[4].TextSize = 3;
	TimeME[4].ItemsCount = 0;
	TimeME[4].ElType = ElementType::TYPE_VARIABLE;
	TimeME[4].Variable = &time.Day;
	TimeME[4].VarType = VariableType::TYPE_LIM_CHAR;
	//Time->Month
	TimeME[5].Previous = &SetME[4];
	TimeME[5].Text = "Month";
	TimeME[5].TextSize = 5;
	TimeME[5].ItemsCount = 0;
	TimeME[5].ElType = ElementType::TYPE_VARIABLE;
	TimeME[5].Variable = &time.Month;
	TimeME[5].VarType = VariableType::TYPE_LIM_CHAR;
	//Time->Year
	TimeME[6].Previous = &SetME[4];
	TimeME[6].Text = "Year";
	TimeME[6].TextSize = 4;
	TimeME[6].ItemsCount = 0;
	TimeME[6].ElType = ElementType::TYPE_VARIABLE;
	TimeME[6].Variable = &time.Year;
	TimeME[6].VarType = VariableType::TYPE_LIM_INT;
	//Time->Apply
	TimeME[7].Previous = &SetME[4];
	TimeME[7].Text = "Apply";
	TimeME[7].TextSize = 5;
	TimeME[7].ItemsCount = 0;
	TimeME[7].ElType = ElementType::TYPE_EXTERNAL;
	TimeME[7].Variable = &TME_Apply;
	TimeME[7].VarType = VariableType::TYPE_VOID;
	//Добавляем детей Time
	SetME[4].Items = TimeME;

	//Stat
	MenuElement *StatME = new MenuElement[4];
	//Stat->Back
	StatME[0].Previous = &SetME[5];
	StatME[0].Text = "Back";
	StatME[0].TextSize = 4;
	StatME[0].ElType = ElementType::TYPE_BACK;
	StatME[0].ItemsCount = 0;
	//Stat->Hour
	StatME[1].Previous = &SetME[5];
	StatME[1].Text = "ShStep";
	StatME[1].TextSize = 6;
	StatME[1].ItemsCount = 0;
	StatME[1].ElType = ElementType::TYPE_VARIABLE;
	StatME[1].Variable = &STAT_ShStep;
	StatME[1].VarType = VariableType::TYPE_INT;
	//Stat->Save
	StatME[2].Previous = &SetME[5];
	StatME[2].Text = "Save";
	StatME[2].TextSize = 4;
	StatME[2].ItemsCount = 0;
	StatME[2].ElType = ElementType::TYPE_EXTERNAL;
	StatME[2].Variable = &StatSettSave;
	StatME[2].VarType = VariableType::TYPE_VOID;
	//Stat->Clear
	StatME[3].Previous = &SetME[5];
	StatME[3].Text = "MemErase";
	StatME[3].TextSize = 8;
	StatME[3].ItemsCount = 0;
	StatME[3].ElType = ElementType::TYPE_EXTERNAL;
	StatME[3].Variable = &StatErase;
	StatME[3].VarType = VariableType::TYPE_VOID;
	//Добавляем детей Stat
	SetME[5].Items = StatME;
	
	//Добавляем детей корня дерева
	RootME[0].Items = SetME;

	//Добавляем в текущую точку входа
	DMenu = RootME[0];
	
}
