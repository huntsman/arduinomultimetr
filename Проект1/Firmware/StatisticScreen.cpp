#include "../Firmware/StatisticScreen.h"

int test[24] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24 };

char DayIndex = 0;
int ColShift = 0;
char DataSet = false;

unsigned char day = 0;
unsigned char month = 0;
unsigned int year = 0;

selmode SelMode = selmode::sel_dataset;
float MaultVal = 0;


StatisticScreen::StatisticScreen()
{
	InterControl = true;
}

void LoadTimeData()
{
	x_tmd di;
	xttotm(&di, N_UDStart + 86400 * DayIndex);
	day = di.tm_mday;
	month = di.tm_mon;
	year = di.tm_year;
}

void MultCalc()
{
	int val;
	for (int i = 0; i < 24; i++)
	{
		if (DataSet)
			val = N_DStat[DayIndex].HStat[i].ALoadPower;
		else
			val = N_DStat[DayIndex].HStat[i].AllPower;

		if (MaultVal < val)
			MaultVal = val;
	}
	MaultVal = 100.0f / MaultVal;
}

void StatisticScreen::ButtonAction(ButtonEvent event)
{
	int ind = 0;
	switch (event.Name)
	{
	case ButtonName::BUTTON_OK:
		if (event.Event == BEventName::CLICK) {
			ind = (int)SelMode + 1;
			if (ind == (int)selmode::sel_end)
				SelMode = selmode::sel_dataset;
			else
			    SelMode = (selmode)ind;
		}else if (event.Event == BEventName::LONG_PUSH)
		{
			Hide = true;
		}
		break;

	case ButtonName::BUTTON_UP:
		if (event.Event == BEventName::CLICK)
			switch (SelMode)
			{
			case selmode::sel_dataset:
				DataSet = !DataSet;
				break;
			case selmode::sel_hour:
				ColShift += STAT_ShStep;
				if (ColShift + DisplayWidth / 3 > 23)
					ColShift = 24 - DisplayWidth / 3;
				break;
			case selmode::sel_data:
				DayIndex++;
				if (DayIndex > SatsticDays - 1)
					DayIndex = SatsticDays - 1;
				break;
			default:
				break;
			}
		break;

	case ButtonName::BUTTON_DOWN:
		if (event.Event == BEventName::CLICK)
			switch (SelMode)
			{
			case selmode::sel_dataset:
				DataSet = !DataSet;
				break;
			case selmode::sel_hour:
				ColShift -= STAT_ShStep;
				if (ColShift < 0)
					ColShift = 0;
				break;
			case selmode::sel_data:
				DayIndex--;
				if (DayIndex < 0)
					DayIndex = 0;
				break;
			default:
				break;
			}
		break;
	}
	MultCalc();
	LoadTimeData();
}

void StatisticScreen::DrawColumn(unsigned char col, unsigned char fillpercent)
{
	int p = fillpercent * 3;
	if (col < DisplayWidth) {
		for (int i = DisplayHeight - GrafShift - 1; i >= 0; i--)
		{
			lcd.setCursor(col, i);
			
			SymbolFill(p);
		}
	}
}

void StatisticScreen::SymbolFill(int &percent)
{
	if (percent >= 100)
		lcd.write(7);
	else if (percent >= 75)
		lcd.write(6);
	else if (percent >= 50)
		lcd.write(5);
	else if (percent >= 25)
		lcd.write(4);
	else 
		lcd.print("  ");

	if (percent >= 100)
		percent -= 100;
	else
		percent = 0;

}

void StatisticScreen::Draw()
{

	int tt = 0;
	float val;


	switch (SelMode)
	{
	case selmode::sel_dataset:
		
		if (DataSet) {
			lcd.setCursor(2, 0);
			lcd.print("<ALoadPower>");
		}
		else {
			lcd.setCursor(3, 0);
			lcd.print("<AllPower>");
		}

		lcd.setCursor(6, 1);
		sprintf(Buffer, "<%02d>", (int)DataSet);
		lcd.print(Buffer);
		break;
	case selmode::sel_hour:
		for (int i = ColShift; i < ColShift + DisplayWidth / 3; i++)
		{
			if (DataSet)
				val = N_DStat[DayIndex].HStat[i].ALoadPower;
			else
				val = N_DStat[DayIndex].HStat[i].AllPower;

			val *= MaultVal;

			DrawColumn(tt, val);
			DrawColumn(tt + 1, val);
			lcd.setCursor(tt, 3);
			lcd.print(i);
			tt += 3;
		}
		break;
	case selmode::sel_data:
		lcd.setCursor(2, 0);
		sprintf(Buffer, "<%02d.%02d.%4d>", day, month, year);
		lcd.print(Buffer);

		lcd.setCursor(6, 1);
		sprintf(Buffer, "<%02d>", DayIndex);
		lcd.print(Buffer);
		break;
	default:
		break;
	}
}
