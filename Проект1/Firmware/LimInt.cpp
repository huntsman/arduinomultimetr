#include "LimInt.h"

LimInt::LimInt()
{
	int_var = 0;
	int_max = 32767;
	int_min = 0;
}
LimInt::LimInt(int min, int max)
{
	int_var = min;
	int_max = max;
	int_min = min;
}
int LimInt::GetValue()
{
	return int_var;
}
void LimInt::SetValue(int val)
{
	if (val < int_min) {
		int_var = int_min;
		return;
	}
	else if (val > int_max) {
		int_var = int_max;
		return;
	}

	int_var = val;
}

void LimInt::IncrValue(int inc)
{
	if (int_var == 0)
		inc = int_min + inc;
	else
		inc += int_var;
	SetValue(inc);
}

/*int LimInt::IncrValue(int inc)
{
	SetValue(int_var + inc);
	return int_var;
}*/