#include "../Firmware/MainScreenOne.h"

MainScreenOne::MainScreenOne() //: Screen()
{
	Hide = false;
}

void MainScreenOne::ButtonAction(ButtonEvent event)
{
}

void MainScreenOne::Draw()
{
	lcd.setCursor(0, 0);
	lcd.print("Voltage:");
	lcd.setCursor(8, 0);
	dtostrf(N_Voltage, 5, 1, Buffer);
	lcd.print(Buffer);
	lcd.setCursor(13, 0);
	lcd.print("V");

	lcd.setCursor(0, 1);
	lcd.print("Current:");
	lcd.setCursor(8, 1);
	dtostrf(N_Current, 5, 1, Buffer);
	lcd.print(Buffer);
	lcd.setCursor(13, 1);
	lcd.print("A");

	lcd.setCursor(0, 2);
	lcd.print("CPower:");
	lcd.setCursor(8, 2);
	dtostrf(N_CurrentPower, 5, 1, Buffer);
	lcd.print(Buffer);
	lcd.setCursor(13, 2);
	lcd.print("W");

	lcd.setCursor(0, 3);
	lcd.print("APower:");
	lcd.setCursor(8, 3);
	dtostrf(N_AllPower, 5, 1, Buffer);
	lcd.print(Buffer);
	lcd.setCursor(13, 3);
	lcd.print("WH");
}
