#ifndef MainScreenOne_h
#define MainScreenOne_h
/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/
#include "Screen.h"

class MainScreenOne : public Screen
{
	public:
		MainScreenOne();
		void Draw();
		void ButtonAction(ButtonEvent event);

};

#endif 