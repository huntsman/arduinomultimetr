#ifndef Symbols_h
#define Symbols_h

const unsigned char SelChar[8] =
{
  0,
  16,
  28,
  31,
  31,
  28,
  16,
  0
};

const unsigned char ColFill25[8] =
{
	0,
	0,
	0,
	0,
	0,
	0,
	31,
	31
};

const unsigned char ColFill50[8] =
{
	0,
	0,
	0,
	0,
	31,
	31,
	31,
	31
};

const unsigned char ColFill75[8] =
{
	0,
	0,
	31,
	31,
	31,
	31,
	31,
	31
};

const unsigned char ColFill100[8] =
{
	31,
	31,
	31,
	31,
	31,
	31,
	31,
	31
};

#endif 