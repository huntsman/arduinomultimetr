#ifndef Base_h
#define Base_h
/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/
#include < math.h >
#include "../Arduino.h"
#include "../EEPROM.h"
#include "LimInt.h"
#include "../LiquidCrystal.h"
#include "AnotherTime.h"

#define  VI_PIN     A7
//#define  VO_PIN     A6
#define  CO_PIN     A4
#define  BUTTON_OK_PIN 16//A2
#define  BUTTON_UP_PIN 15//A1
#define  BUTTON_DOWN_PIN 14//A0

//#define  PWM_CHARGE_PIN    5
#define  PWM_LCD_LED_PIN    3
#define  LOAD_SWITCH_PIN    17//A3

#define SatsticDays 4 // Max days  
//#define SatsticHour SatsticDays * 24 // Max hourses

#define DisplayWidth 16 //Ширина
#define DisplayHeight 4 //Высота

#define FCAdress 0				 // Forced Cleaning
#define SSAdress 1				 // Settings Save Adress
#define CUTAdress 40			 // Current Unix Time
#define APAdress CUTAdress + 4	 // (CUTAdress 2 byte) All Power
#define ALPAdress APAdress + 2	 // (APAdress 2 byte)  All Load Power
#define SUTAdress ALPAdress + 2	 // Staistic Unix Time
#define HSStart SUTAdress + 4	 // (SUTAdress 4 byte) Block Start Position
#define HSSize 4			     // (s_all_power 2 byte + s_aload_power 2 byte) Data block size in bytes

enum class ButtonName  : int
{
	NONE,           //Заглушка
	BUTTON_OK,
	BUTTON_UP,
	BUTTON_DOWN
};

enum class BEventName : int
{
	NONE,           //Заглушка
	CLICK,
	LONG_PUSH
};

struct ButtonEvent
{         
	ButtonName Name;
	BEventName Event;
}; 

struct HourStat
{
	int AllPower;
	int ALoadPower;
};
struct DayStat
{
	HourStat HStat[24];
};

extern AnotherTime time;
extern LiquidCrystal lcd;

extern float ADC_VoltMult;
extern float ADC_VoltZePo;
extern float ADC_CurrMult;
extern int ADC_CurrZePo;

extern int ADC_AverDepth;
extern float ADC_SmoothCoof;

extern bool LCD_LedOnStart;
extern bool LCD_LedEnable;
extern LimInt LCD_Bright;

extern bool LOAD_On;
extern float LOAD_Power;

extern int STAT_ShStep;

extern float N_Voltage;
extern float N_Current;
extern float N_CurrentPower;
extern float N_AllPower;
extern float N_ALoadPower;
extern DayStat N_DStat[];
extern unsigned long N_UDStart;

#endif 