#include "AnotherTime.h"

void AnotherTime::ExMass()
{
	etimer *t = new etimer[TCount + 1];
	if (TCount > 0) {
		for (int i = 0; i < TCount; i++)
			t[i] = Timers[i];
		delete[] Timers;
	}
	Timers = t;
	++TCount;
}

void AnotherTime::AddTimer(unsigned long millis, void(*event)())
{
	ExMass();
	etimer t;
	t.Event = event;
	t.Period = millis;
	t.LastMillis = 0;
	Timers[TCount - 1] = t;
}

unsigned long tu_lm;
void AnotherTime::Update(unsigned long millis)
{
	if (LastMillis == 0 || millis < LastMillis)
		LastMillis = millis;

	tu_lm = millis - LastMillis;
	if (tu_lm > 999) {
		IncrSec();
		LastMillis = (tu_lm - 1000) + millis;
	}

	for (int i = 0; i < TCount; i++) {
		if (LastMillis == 0 || millis < LastMillis)
			LastMillis = millis;

		if (millis - Timers[i].LastMillis >= Timers[i].Period) {
			(*Timers[i].Event)();
			Timers[i].LastMillis = millis;
		}
	}
}

void AnotherTime::SyncPush()
{
	x_tmd tmd;
	tmd.tm_sec = (unsigned char)Seconds.GetValue();
	tmd.tm_min = (unsigned char)Minutes.GetValue();
	tmd.tm_hour = (unsigned char)Hours.GetValue();
	tmd.tm_mday = (unsigned char)Day.GetValue();
	tmd.tm_mon = (unsigned char)Month.GetValue();
	tmd.tm_year = (unsigned int)Year.GetValue();
	UnixTime = (unsigned long)xtmtot(&tmd);
}

void AnotherTime::SyncPull()
{
	x_tmd tmd;
	xttotm(&tmd, UnixTime);
	Seconds.SetValue(tmd.tm_sec);
	Minutes.SetValue(tmd.tm_min);
	Hours.SetValue(tmd.tm_hour);
	Day.SetValue(tmd.tm_mday);
	Month.SetValue(tmd.tm_mon);
	Year.SetValue(tmd.tm_year);
	
}

void AnotherTime::IncrSec()
{
	Seconds.IncrValue(1);
	++UnixTime;
	if (Seconds.GetValue() > 59) {
		Seconds.SetValue(0);

		Minutes.IncrValue(1);
		if (Minutes.GetValue() > 59) 
			SyncPull();
	}
}

unsigned int AnotherTime::DayDiff(unsigned long ut)
{
	return (UnixTime - ut) / (unsigned int)86400;
}