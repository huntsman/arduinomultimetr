#include "LimChar.h"

LimChar::LimChar()
{
	char_var = 0;
	char_max = 127;
	char_min = 0;
}
LimChar::LimChar(char min, char max)
{
	char_var = min;
	char_max = max;
	char_min = min;
}
char LimChar::GetValue()
{
	return char_var;
}
void LimChar::SetValue(char val)
{
	if (val < char_min) {
		char_var = char_min;
		return;
	}
	else if (val > char_max) {
		char_var = char_max;
		return;
	}

	char_var = val;
}

void LimChar::IncrValue(char inc)
{
	if (char_var == 0)
		inc = char_min + inc;
	else
		inc += char_var;
	SetValue(inc);
}

/*int LimInt::IncrValue(bool plus)
{
	SetValue(int_var + inc);
	return int_var;
}*/