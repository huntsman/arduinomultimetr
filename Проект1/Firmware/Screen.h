#ifndef Screen_h
#define Screen_h
/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/
#include "Base.h"

class Screen
{
public:
	Screen();
	virtual void Draw();
	virtual void ButtonAction(ButtonEvent event);
	bool Hide;
	bool InterControl;
protected:
	char Buffer[16];

};

#endif 