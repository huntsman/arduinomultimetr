#include "../Firmware/MainScreenTwo.h"

MainScreenTwo::MainScreenTwo()
{
	Hide = false;
}

void MainScreenTwo::ButtonAction(ButtonEvent event)
{
}

/*int freeRam() {
	extern int __heap_start, *__brkval;
	int v;
	return (int)&v - (__brkval == 0 ? (int)&__heap_start : (int)__brkval);
}*/

/*void MainScreenTwo::Draw()
{
	lcd.setCursor(0, 0);
	lcd.print("Hello:)");
	lcd.setCursor(0, 1);
	lcd.print("FreeRam:");
	lcd.setCursor(8, 1);
	sprintf(Buffer, "%d", freeRam());
	lcd.print(Buffer);
	
}*/
void MainScreenTwo::Draw()
{
	lcd.setCursor(0, 0);
	sprintf(Buffer, "%02d:%02d:%02d", time.Hours.GetValue(), time.Minutes.GetValue(), time.Seconds.GetValue());
	lcd.print(Buffer);

	lcd.setCursor(0, 1);
	sprintf(Buffer, "%02d.%02d.%04d", time.Day.GetValue(), time.Month.GetValue(), time.Year.GetValue());
	lcd.print(Buffer);

	/*lcd.setCursor(0, 2);
	sprintf(Buffer, "%s %d", "FreeRam:", freeRam());
	lcd.print(Buffer);*/

	lcd.setCursor(0, 3);
	sprintf(Buffer, "%s %d", "UT:", time.UnixTime);
	lcd.print(Buffer);
}