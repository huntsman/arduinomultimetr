#ifndef StrangeTimer_h
#define StrangeTimer_h
/*
#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
#endif 
*/
class StrangeTimer
{
	public:
		StrangeTimer();
		StrangeTimer(unsigned long millis, void (*Event)());
		void Start();
		void Stop();
		void SetPeriod(unsigned long millis);
		void SetEvent(void(*event)());
		void Update(unsigned long millis);
		bool init;
	private:
		unsigned long Time;
		void (*Event)();
		unsigned long LastMillis;
		bool StopTimer;
};

#endif 