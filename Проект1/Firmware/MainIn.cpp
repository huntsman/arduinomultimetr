#include "../Firmware/MainIn.h"
#include "Symbols.h"

MainIn::MainIn(){}

//////////Arduino//////////
//////////////////////////
/////////////////////////
//#include "StrangeTimer.h"
//#include "Base.h"
#include "Interface.h"
#include "Adc.h"
#include "LoadManager.h"
#include "Statistic.h"

Statistic Stat;
LoadManager LManager;
Interface Interf;
Adc adc;


void ButtonTimerUpdate(){
	Interf.InputUpdate();}

void DrawTimerUpdate(){
	Interf.DrawUpdate();}

void AdcTimerUpdate(){
	adc.Update();}

void StatTimerUpdate(){
	Stat.Collect();}

unsigned char lasthours = 255;
void SaveStatistic() {
	if (time.Hours.GetValue() != lasthours) {
		if (lasthours != 255)
			Stat.Save();
		lasthours = time.Hours.GetValue();
	}
}

void LoadTimerUpdate() {
	LManager.Update();
	SaveStatistic();
}

void MainIn::setup()
{
	time.AddTimer(50, ButtonTimerUpdate);
	time.AddTimer(100, AdcTimerUpdate);
	time.AddTimer(150, StatTimerUpdate);
	time.AddTimer(200, DrawTimerUpdate);
	time.AddTimer(60000, LoadTimerUpdate);
	
	lcd.begin(16, 4);
	lcd.createChar(1, SelChar);
	lcd.createChar(4, ColFill25);
	lcd.createChar(5, ColFill50);
	lcd.createChar(6, ColFill75);
	lcd.createChar(7, ColFill100);

	Load();
}

void MainIn::loop()
{
	time.Update(millis());
}

void MainIn::Save()
{

}

void MainIn::Load()
{
	EEPROM.get(SSAdress, ADC_VoltMult);
	EEPROM.get(SSAdress + 4, ADC_VoltZePo);
	EEPROM.get(SSAdress + 4 + 4, ADC_CurrMult);
	EEPROM.get(SSAdress + 4 + 4 + 4, ADC_CurrZePo);
	EEPROM.get(SSAdress + 4 + 4 + 4 + 2, ADC_AverDepth);
	EEPROM.get(SSAdress + 4 + 4 + 4 + 2 + 2, ADC_SmoothCoof);
	EEPROM.get(SSAdress + 20, LCD_LedOnStart);
	EEPROM.get(SSAdress + 20 + 1, LCD_LedEnable);
	int b = 0;
	EEPROM.get(SSAdress + 20 + 1 + 1, b);
	LCD_Bright.SetValue(b);
	EEPROM.get(SSAdress + 24, LOAD_On);
	EEPROM.get(SSAdress + 24 + 1, LOAD_Power);
	EEPROM.get(SSAdress + 29, STAT_ShStep);

	Stat.Load();
}
