#ifndef LimInt_h
#define LimInt_h

class LimInt
{
public:
	LimInt(void);
	LimInt(int min, int max);
	int GetValue();
	void SetValue(int val);
	void IncrValue(int inc);
	//int IncrValue(int inc);
private:
	int int_var;
	int int_max;
	int int_min;

};

#endif 