#include "../Firmware/Base.h"

AnotherTime time;
LiquidCrystal lcd(11, 10, 9, 8, 7, 6);

float ADC_VoltMult  = 0.027f;
float ADC_VoltZePo = 0.0f;
float ADC_CurrMult = 0.047f;
int ADC_CurrZePo = 509;

int ADC_AverDepth = 256;
float ADC_SmoothCoof = 0.3f;

bool LCD_LedOnStart = false;
bool LCD_LedEnable = false;
LimInt LCD_Bright(600, 1023);

bool LOAD_On = false;
float LOAD_Power = 0.0f;

int STAT_ShStep = 4;

float N_Voltage = 0.0f;
float N_Current = 0.0f;
float N_CurrentPower = 0.0f;
float N_AllPower = 0.0f;
float N_ALoadPower = 0.0f;
DayStat N_DStat[SatsticDays] = { 0 };
unsigned long N_UDStart = 0;

