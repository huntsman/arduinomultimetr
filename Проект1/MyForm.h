#pragma once
#include "Firmware/Base.h"
#include "Firmware/MainIn.h"
#include "DeviceBase.h"
#include "Arduino.h"
#include <string>

namespace ������1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	MainIn ArduinoMain;
	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  OkButton;
	public: System::Windows::Forms::TextBox^  MainLcd;
	protected:

	private: System::Windows::Forms::Button^  UpButton;
	private: System::Windows::Forms::Button^  DownButton;
	private: System::Windows::Forms::Timer^  MainTimer;
	private: System::Windows::Forms::Timer^  MTimer;

	private: System::ComponentModel::IContainer^  components;



	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->OkButton = (gcnew System::Windows::Forms::Button());
			this->MainLcd = (gcnew System::Windows::Forms::TextBox());
			this->UpButton = (gcnew System::Windows::Forms::Button());
			this->DownButton = (gcnew System::Windows::Forms::Button());
			this->MainTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->MTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->SuspendLayout();
			// 
			// OkButton
			// 
			this->OkButton->Location = System::Drawing::Point(307, 12);
			this->OkButton->Name = L"OkButton";
			this->OkButton->Size = System::Drawing::Size(30, 38);
			this->OkButton->TabIndex = 0;
			this->OkButton->Text = L"Ok";
			this->OkButton->UseVisualStyleBackColor = true;
			this->OkButton->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::OkButton_MouseDown);
			this->OkButton->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::OkButton_MouseUp);
			// 
			// MainLcd
			// 
			this->MainLcd->AcceptsReturn = true;
			this->MainLcd->Font = (gcnew System::Drawing::Font(L"Courier New", 20, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->MainLcd->Location = System::Drawing::Point(12, 12);
			this->MainLcd->MaxLength = 64;
			this->MainLcd->Multiline = true;
			this->MainLcd->Name = L"MainLcd";
			this->MainLcd->ReadOnly = true;
			this->MainLcd->Size = System::Drawing::Size(271, 127);
			this->MainLcd->TabIndex = 1;
			this->MainLcd->Text = L"1234567890123456\r\n1234567890123456\r\n1234567890123456\r\n1234567890123456";
			this->MainLcd->WordWrap = false;
			// 
			// UpButton
			// 
			this->UpButton->Location = System::Drawing::Point(307, 56);
			this->UpButton->Name = L"UpButton";
			this->UpButton->Size = System::Drawing::Size(30, 38);
			this->UpButton->TabIndex = 2;
			this->UpButton->Text = L"U";
			this->UpButton->UseVisualStyleBackColor = true;
			this->UpButton->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::UpButton_MouseDown);
			this->UpButton->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::UpButton_MouseUp);
			// 
			// DownButton
			// 
			this->DownButton->Location = System::Drawing::Point(307, 101);
			this->DownButton->Name = L"DownButton";
			this->DownButton->Size = System::Drawing::Size(30, 38);
			this->DownButton->TabIndex = 3;
			this->DownButton->Text = L"D";
			this->DownButton->UseVisualStyleBackColor = true;
			this->DownButton->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::DownButton_MouseDown);
			this->DownButton->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::DownButton_MouseUp);
			// 
			// MainTimer
			// 
			this->MainTimer->Enabled = true;
			this->MainTimer->Tick += gcnew System::EventHandler(this, &MyForm::timer1_Tick);
			// 
			// MTimer
			// 
			this->MTimer->Enabled = true;
			this->MTimer->Interval = 10;
			this->MTimer->Tick += gcnew System::EventHandler(this, &MyForm::MTimer_Tick);
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(354, 156);
			this->Controls->Add(this->DownButton);
			this->Controls->Add(this->UpButton);
			this->Controls->Add(this->MainLcd);
			this->Controls->Add(this->OkButton);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"MyForm";
			this->Text = L"Device Test";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion


			 
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) 
	{
		MainLcd->Clear();

		for(int i = 0; i < lcd.Max_Row; i++)
		{
			MainLcd->Text += CharToSysString(lcd.Text[i]);
			MainLcd->Text += "\r\n";
		}
	}

	private: System::Void MTimer_Tick(System::Object^  sender, System::EventArgs^  e)
	{
		ArduinoMain.loop();
		MBTimer += MTimer->Interval;
		InterrTimer.Update(MBTimer);
	}

	// char* to System::String^
	System::String^ CharToSysString(char* ch)
	{
		System::String^ str;
		for (int i = 0; ch[i] != '\0'; i++)
		{
			str += wchar_t(ch[i]);
		}
		return str;
	}


private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
	ArduinoMain.setup();
}

private: System::Void OkButton_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	SetPinPotential(A2, 5);
}
private: System::Void OkButton_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	SetPinPotential(A2, 0);
}
private: System::Void UpButton_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	SetPinPotential(A1, 5);
}
private: System::Void UpButton_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	SetPinPotential(A1, 0);
}
private: System::Void DownButton_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	SetPinPotential(A0, 5);
}
private: System::Void DownButton_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	SetPinPotential(A0, 0);
}
};
}
