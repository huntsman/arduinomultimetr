#include "MsTimer2.h"

void MsTimer2::set(unsigned long ms, void (*f)()) {
	InterrTimer.SetPeriod(ms);
	InterrTimer.SetEvent(f);
}

void MsTimer2::start() {
	InterrTimer.Start();
}

void MsTimer2::stop() {
	InterrTimer.Stop();
}





