#pragma once
#include "DeviceBase.h"

namespace ������1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Collections::Generic;
	
	public ref class A_Pin
	{
	public:
		A_Pin(char* name, float * var) {Name = gcnew String(name); Variable = var; };
		property String^ Name;
		property float * Variable;
	};
	
	/// <summary>
	/// ������ ��� MyForm1
	/// </summary>
	public ref class MyForm1 : public System::Windows::Forms::Form
	{
	public:
		MyForm1(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::ListBox^  listBox1;
	private: System::Windows::Forms::TrackBar^  trackBar1;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Button^  button1;
	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->listBox1 = (gcnew System::Windows::Forms::ListBox());
			this->trackBar1 = (gcnew System::Windows::Forms::TrackBar());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->BeginInit();
			this->SuspendLayout();
			// 
			// listBox1
			// 
			this->listBox1->FormattingEnabled = true;
			this->listBox1->Location = System::Drawing::Point(31, 31);
			this->listBox1->Name = L"listBox1";
			this->listBox1->Size = System::Drawing::Size(120, 95);
			this->listBox1->TabIndex = 0;
			this->listBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm1::listBox1_SelectedIndexChanged);
			// 
			// trackBar1
			// 
			this->trackBar1->Location = System::Drawing::Point(27, 171);
			this->trackBar1->Maximum = 50;
			this->trackBar1->Name = L"trackBar1";
			this->trackBar1->Size = System::Drawing::Size(124, 45);
			this->trackBar1->TabIndex = 1;
			this->trackBar1->Value = 25;
			this->trackBar1->Scroll += gcnew System::EventHandler(this, &MyForm1::trackBar1_Scroll);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(28, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(51, 13);
			this->label1->TabIndex = 2;
			this->label1->Text = L"������:";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(31, 145);
			this->textBox1->Name = L"textBox1";
			this->textBox1->ReadOnly = true;
			this->textBox1->Size = System::Drawing::Size(120, 20);
			this->textBox1->TabIndex = 3;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(28, 129);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(74, 13);
			this->label2->TabIndex = 4;
			this->label2->Text = L"����������:";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(51, 215);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 5;
			this->button1->Text = L"���������";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm1::button1_Click);
			// 
			// MyForm1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(189, 250);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->trackBar1);
			this->Controls->Add(this->listBox1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"MyForm1";
			this->Text = L"PinVarEditor";
			this->Load += gcnew System::EventHandler(this, &MyForm1::MyForm1_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackBar1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		List <A_Pin^>^ Pins = gcnew List <A_Pin^>();

	private: System::Void trackBar1_Scroll(System::Object^  sender, System::EventArgs^  e) {
		textBox1->Text = Convert::ToString(trackBar1->Value * 0.1f);
	}
	private: System::Void MyForm1_Load(System::Object^  sender, System::EventArgs^  e) {
		A_Pin^ ppp;
		ppp = gcnew A_Pin("A0Variable", &A0Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("A1Variable", &A1Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("A2Variable", &A2Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("A3Variable", &A3Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("A4Variable", &A4Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("A5Variable", &A5Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("A6Variable", &A6Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("A7Variable", &A7Variable);
		Pins->Add(ppp);

		ppp = gcnew A_Pin("D2Variable", &D2Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D3Variable", &D3Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D4Variable", &D4Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D5Variable", &D5Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D6Variable", &D6Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D7Variable", &D7Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D8Variable", &D8Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D9Variable", &D9Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D10Variable", &D10Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D11Variable", &D11Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D12Variable", &D12Variable);
		Pins->Add(ppp);
		ppp = gcnew A_Pin("D13Variable", &D13Variable);
		Pins->Add(ppp);
		//
		listBox1->DataSource = Pins;
		listBox1->DisplayMember = "Name";
		//listBox1->ValueMember = "Variable";
		listBox1->SelectedIndex = 0;
	}
	private: System::Void listBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
		ListBox ^ lb = (ListBox ^)sender;
		A_Pin ^ ap = (A_Pin^)lb->SelectedValue;
		float tf = *ap->Variable;
		if (tf == 0)
			trackBar1->Value = 0;
		else
			trackBar1->Value = (int)(tf / 0.1f);
		textBox1->Text = Convert::ToString(tf);
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		float tf = float::Parse(textBox1->Text);
		float * stf = ((A_Pin^)listBox1->SelectedItem)->Variable;
		*stf = tf;
	}
};
}
