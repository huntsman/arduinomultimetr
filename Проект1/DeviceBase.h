#ifndef DeviceBase_h
#define DeviceBase_h

//#include "Arduino.h"
#include "Firmware\StrangeTimer.h"

extern float A7Variable;
extern float A6Variable;
extern float A5Variable;
extern float A4Variable;
extern float A3Variable;
extern float A2Variable;
extern float A1Variable;
extern float A0Variable;

extern float D13Variable;
extern float D12Variable;
extern float D11Variable;
extern float D10Variable;
extern float D9Variable;
extern float D8Variable;
extern float D7Variable;
extern float D6Variable;
extern float D5Variable;
extern float D4Variable;
extern float D3Variable;
extern float D2Variable;

extern unsigned long MBTimer;
extern StrangeTimer InterrTimer;

static void SetPinPotential(int pin, float val)
{
	if (val > 5)
		val = 5;
	else if (val < 0)
		val = 0;

	switch (pin)
	{
	case 14:
		A0Variable = val;
		break;
	case 15:
		A1Variable = val;
		break;
	case 16:
		A2Variable = val;
		break;
	case 17:
		A3Variable = val;
		break;
	case 18:
		A4Variable = val;
		break;
	case 19:
		A5Variable = val;
		break;
	case 20:
		A6Variable = val;
		break;
	case 21:
		A7Variable = val;
		break;

	case 2:
		D2Variable = val;
		break;
	case 3:
		D3Variable = val;
		break;
	case 4:
		D4Variable = val;
		break;
	case 5:
		D5Variable = val;
		break;
	case 6:
		D6Variable = val;
		break;
	case 7:
		D7Variable = val;
		break;
	case 8:
		D8Variable = val;
		break;
	case 9:
		D9Variable = val;
		break;
	case 10:
		D10Variable = val;
		break;
	case 11:
		D11Variable = val;
		break;
	case 12:
		D12Variable = val;
		break;
	case 13:
		D13Variable = val;
		break;
	}
}

#endif 